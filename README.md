# Body-Mass Index (BMI) Calculator

Body Mass Index is a simple calculation using a person’s height and weight.
The formula is BMI = kg/m2 where kg is a person’s weight in kilograms and m2 is their height in meters squared. 
A BMI of 25.0 or more is overweight, while the healthy range is 18.5 to 24.9.

## Usage

Use the package manager to install [httpie](https://httpie.io/).

```bash
$ dnf install httpie
$ echo '{ "height": "180", "weight": "58" }' | http https://bmi.blackbox.id/api/bmicalculator/
```

## API URL

[https://bmi.blackbox.id/api/bmicalculator](https://bmi.blackbox.id/api/bmicalculator/)

## Action Demo

[![asciicast](https://asciinema.org/a/q4ymQFy4zF2HeGtdncA7rEZFw.png)](https://asciinema.org/a/q4ymQFy4zF2HeGtdncA7rEZFw)

## Security Implementation

```
SAST using Bandit from Gitlab-CI
Container Scanning using Gitlab-CI
NGINX + HTTPS SSL Let's Encrypt
```
