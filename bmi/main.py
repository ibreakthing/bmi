#!/usr/bin/env python
from typing import Optional
from fastapi import FastAPI
from fastapi.responses import JSONResponse
from pydantic import BaseModel, PositiveFloat

class Item(BaseModel):
    weight: PositiveFloat
    height: PositiveFloat

app = FastAPI()

@app.get("/")
async def root():
    return {
        "Hello"
        "message": "Body mass index (BMI) is a measurement of a person's weight in relation to their height. It offers an inexpensive and simple method of categorising people according to their BMI value so that we can screen people’s weight category and indicate their potential risk for health conditions.",
        "height": "In Centimeters eg: 150",
        "weight": "In Kg",
        "underweight": "BMI less than 18.5",
        "normal_healthy_weight": "BMI between 18.5 and 24.9",
        "overweight": "BMI between 25.0 and 29.9",
        "obese": "BMI between 30.0 and 39.9",
        "morbidly_obese": "BMI 40.0 and above"
    }

@app.post("/api/bmicalculator/")
async def bmicalculator(item: Item):
    
    bmi = round(item.weight / ((item.height/100)**2), 2)
    
    if bmi < 18.5:
        return JSONResponse(
            content={
                "bmi": bmi,
                "label": "Underweight",
            },
            status_code=200)
    elif bmi >= 18.5 and bmi < 24.9:
        return JSONResponse(
            content={
                "bmi": bmi,
                "label": "Normal weight",
                },
            status_code=200)
    elif bmi >= 25.0 and bmi < 29.9:
        return JSONResponse(
            content={
                "bmi": bmi,
                "label": "Overweight",
                },
            status_code=200)
    elif bmi >= 30.0 and bmi < 39.9:
        return JSONResponse(
            content={
                "bmi": bmi,
                "label": "Obese",
                },
            status_code=200)
    elif bmi >= 40.0:
        return JSONResponse(
            content={
                "bmi": bmi,
                "label": "Morbidly Obese",
                },
            status_code=200)
